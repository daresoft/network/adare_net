
to compile without debug information:
make <enter>

to compile with debug information:
make all-debug <enter>

to clean files:
make clean <enter>

Thanks God and Mary and Cia!!! :-D
Enjoy!!! :-D

Obs.: in a production servers the incomming socket will need a timer
for incoming data. this is made or by a protected type entry or task type
entry with a selective select or by the use of the polls package with a
time_out greater than zero in function start_events_listen(). It is up to you
to develop them but an example with selective select is showed below. :-)

-x-
declare
  Mi_protected  : mi_receive_protect;
begin
  ok := False;

  select
    Mi_protected.entry_receive_buffer (incoming_socket, Buffer, received_size);
    ok := True;
  or
    delay 2.0; -- 2 seconds
  end select;

  if not ok then
  --
  end if;
end;

-x-
protected type mi_receive_protect is

  entry entry_receive_buffer (sock : socket; buffer : in out socket_buffer;
    received_size : out ssize_t);

end mi_receive_protect;

-x-
protected body mi_receive_protect is

  entry entry_receive_buffer (sock : socket; buffer : in out socket_buffer;
    received_size : out ssize_t)
  is
  begin
    received_size := receive_buffer (sock, buffer); -- block
  end entry_receive_buffer;

end mi_receive_protect;

-x-

Enjoy!! :-D
