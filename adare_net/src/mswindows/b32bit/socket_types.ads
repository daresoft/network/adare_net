
with Interfaces.C;
use Interfaces, Interfaces.C;

package socket_types
  with  Pure
is

  type socket_type is new Unsigned_32;

  invalid_socket  : constant socket_type  := -1;

  type ssize_t is new Integer_32;

  socket_error  : constant ssize_t  := -1;

  type socklen_t is new Integer_32;

  send_receive_len : constant int := int'Last - 1;

end socket_types;
